import {MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME} from "./config";
import {texts} from "../data";

let Users = new Map();
let Rooms = new Map();

function RoomsArr(Map) {
  let arr = [];
  Map.forEach( (info, name) => {
    if(info.status === 'open') {
      arr.push({
        key: name,
        value: info
      });
    }
  });
  return arr;
}

function UsersArr(Map) {
  let arr = [];
  Map.forEach( (info, name) => {
      arr.push({
        key: name,
        value: info
      });
  });
  return arr;
}

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    if (Users.has(username)) {
      socket.emit('LOGGED_USER', username);
    }
    else {
      Users.set(username, {
        progress: 0,
        isReady: false
      });
    }

    socket.emit('UPDATE_ROOMS_CARDS', RoomsArr(Rooms));

    socket.on('ADD_ROOM', (roomName) => {
      let validRoomNameReg = /^[a-z][a-z0-9]/i;
      if(!validRoomNameReg.test(roomName) || Rooms.has(roomName)) {
        socket.emit('INVALID_ROOM_NAME');
      } else {
        Rooms.set(roomName, {
          userNum: 0,
          status: 'open',
          users: null,
          finishers: null
        });
        socket.emit('VALID_ROOM_NAME', roomName);
      }
    });

    socket.on('JOIN_ROOM', (roomName) => {
      socket.join(roomName, () => {
        let newInfo = Rooms.get(roomName);
        newInfo.userNum++;
        if(newInfo.userNum === MAXIMUM_USERS_FOR_ONE_ROOM) {
          newInfo.status = 'closed';
        }
        if (newInfo.users) {
          newInfo.users.push(username);

        } else {
          newInfo.users = [username];
        }
        Rooms.set(roomName, newInfo);
        io.emit('UPDATE_ROOMS_CARDS', RoomsArr(Rooms));
        io.to(roomName).emit('UPDATE_ROOM',roomName, RoomsArr(Rooms), UsersArr(Users));
      });
    });

    socket.on('LEAVE_ROOM', (username) => {
      const roomName = Object.keys(socket.rooms)[1];
      let newInfo = Rooms.get(roomName);
      newInfo.userNum--;
      if (newInfo.userNum !== 0) {
        if (newInfo.userNum < MAXIMUM_USERS_FOR_ONE_ROOM) {
          newInfo.status = 'open';
        }
        newInfo.users.splice(newInfo.users.findIndex(name => name === username), 1);
        Rooms.set(roomName, newInfo);
      } else {
        Rooms.delete(roomName);
      }
      socket.leave(roomName);
      io.emit('UPDATE_ROOMS_CARDS', RoomsArr(Rooms));
      io.to(roomName).emit('UPDATE_ROOM',roomName, RoomsArr(Rooms), UsersArr(Users));
      if (newInfo.userNum !== 0) {
        const notReadyUser = Rooms.get(roomName).users.find(name => !Users.get(name).isReady);
        if (!notReadyUser) {
          io.to(roomName).emit('START_TIMER', roomName, SECONDS_TIMER_BEFORE_START_GAME);
        }
      }
    });

    socket.on('USER_READY', (username) => {
      let newInfo = Users.get(username);
      newInfo.isReady = !newInfo.isReady;
      Users.set(username, newInfo);
      const roomName = Object.keys(socket.rooms)[1];
      io.to(roomName).emit('UPDATE_ROOM',roomName, RoomsArr(Rooms), UsersArr(Users));
      const notReadyUser = Rooms.get(roomName).users.find(name => !Users.get(name).isReady);
      if (!notReadyUser) {
        io.to(roomName).emit('START_TIMER', roomName, SECONDS_TIMER_BEFORE_START_GAME);
      }
    });

    socket.on('START_GAME', () => {
      const roomName = Object.keys(socket.rooms)[1];
      let newInfo = Rooms.get(roomName);
      newInfo.status = 'closed';
      Rooms.set(roomName, newInfo);
      io.emit('UPDATE_ROOMS_CARDS', RoomsArr(Rooms));
      const num = Math.round(Math.random()*texts.length);
      setTimeout(() => {socket.emit('VIEW_GAME',  texts[num])}, SECONDS_TIMER_BEFORE_START_GAME*1000);
    });

    socket.on('UPDATE_USER_PROGRESS', (username, progress) => {
      let newInfo = Users.get(username);
      newInfo.progress = progress;
      Users.set(username, newInfo);
      const roomName = Object.keys(socket.rooms)[1];
      io.to(roomName).emit('UPDATE_ROOM',roomName, UsersArr(Rooms), UsersArr(Users));
    });

    socket.on('FINISH_GAME', username => {
      const roomName = Object.keys(socket.rooms)[1];
      let newInfo = Rooms.get(roomName);
      if (newInfo.finishers) {
        newInfo.finishers.push(username);
        Rooms.set(roomName, newInfo);
      }
      else {
        newInfo.finishers = [username];
      }
      if (newInfo.users.length === newInfo.finishers.length) {
        io.to(roomName).emit('END_GAME', newInfo);
      }
    });


    socket.on('disconnect', () => {
      Users.delete(username);

    });
  });
};
