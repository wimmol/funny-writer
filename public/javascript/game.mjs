import {createRoomPrew, createProgressList, addClass, removeClass, createGameText} from "./helper.mjs";

const username = sessionStorage.getItem("username");
const socket = io("", { query: { username } });

if (!username) {
  window.location.replace("/login");
}

const roomsPage = document.getElementById('rooms-page')
const createRoomButton = document.getElementById('create-room-button')
const roomsList = document.getElementById('rooms-list');
const gamePage = document.getElementById('game-page');
const roomNameH1 = document.getElementById('room-name');
const leaveRoomButton = document.getElementById('leave-room-button');
const readyButton = document.getElementById('ready-button');
const timerEl = document.getElementById('timer');
const gameWrap = document.getElementById('game-view-wrapper');

readyButton.addEventListener('click', userReady);
createRoomButton.addEventListener('click', createNewRoom);
leaveRoomButton.addEventListener('click', leaveRoom)


function createNewRoom() {
  let roomName = prompt('Enter room name');
  socket.emit('ADD_ROOM', roomName);
}

function leaveRoom() {
  socket.emit('LEAVE_ROOM', username);
  window.location.replace("/game");
}

function userReady() {
  socket.emit('USER_READY', username);
  if (readyButton.value === 'Ready') {
    readyButton.value = 'Not Ready';
  } else {
    readyButton.value = 'Ready';
  }
}

function joinRoom(roomName) {
  if(typeof roomName !== 'string'){
    roomName = roomName.target.id;
  }
  addClass(roomsPage, 'display-none')
  gamePage.style.display = 'flex';
  roomNameH1.innerText = roomName;
  socket.emit('JOIN_ROOM', roomName);
}

function showRooms(Rooms) {
  roomsList.innerHTML = '';
  Rooms.forEach(room => {
    createRoomPrew(room.key, roomsList, room.value.userNum);
    const joinButton = document.getElementById(room.key);
    joinButton.addEventListener('click', joinRoom);
  });
}

function reLogIn(username) {
  alert(`${username} is already logged in`);
  sessionStorage.removeItem("username");
  window.location.replace("/login");
}

function invalidName() {
  alert('This room name is not valid');
}

function validName(roomName) {
  const joinRoomButton = createRoomPrew(roomName, roomsList);
  joinRoomButton.addEventListener('click', joinRoom);
  joinRoom(roomName);
}

function updateRoom(roomName, Rooms, Users) {
  const userList = document.getElementById('user-list');
  userList.innerHTML = '';
  createProgressList(username, roomName, Rooms, Users);
}

function startTimer(roomName, timerSec) {
  addClass(readyButton, 'display-none');
  removeClass(timerEl, 'display-none');
  for(let i = 0; i < timerSec; i++) {
    setTimeout(() => {
      timerEl.innerText = `${timerSec - i}`;
    }, i*1000);
  }
  socket.emit('START_GAME');
  setTimeout(() => {
    addClass(timerEl, 'display-none');
    addClass(leaveRoomButton, 'display-none');
  }, timerSec*1000);
}

function startGame(text) {
  let textArr = text.split('');
  gameWrap.append(createGameText(textArr));
  let textArrLen = textArr.length;
  let marker = 0;
  const chars = document.getElementsByClassName('game-char');
  document.addEventListener('keydown', (event) => {
    if(event.key === textArr[marker]) {
      chars[marker].style = 'background-color: chartreuse';
      marker++;
      socket.emit('UPDATE_USER_PROGRESS', username, marker*100/textArrLen);
    }
    if (marker === textArrLen) {
      socket.emit('FINISH_GAME', username);
    }
  });

}

socket.on('LOGGED_USER', reLogIn);
socket.on('UPDATE_ROOMS_CARDS', showRooms);
socket.on('INVALID_ROOM_NAME', invalidName);
socket.on('VALID_ROOM_NAME', validName);
socket.on('UPDATE_ROOM', updateRoom);
socket.on('START_TIMER', startTimer);
socket.on('VIEW_GAME', startGame);
socket.on('END_GAME', info => console.log(info));