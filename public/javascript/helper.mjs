export const createElement = ({ tagName, className, attributes = {} }) => {
  const element = document.createElement(tagName);

  if (className) {
    addClass(element, className);
  }

  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

  return element;
};

export const addClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.add(...classNames);
};

export const removeClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.remove(...classNames);
};

export const formatClassNames = className => className.split(" ").filter(Boolean);


export function createRoomPrew(roomName, roomsList, usersNum = 1) {
  const room = createElement({
    tagName: 'div',
    className: 'room',
    attributes: {
      id: `${roomName}-card`
    }
  });
  const roomNameT = createElement({
    tagName: 'h2',
    className: 'room-name'
  });
  roomNameT.innerText = roomName;
  const roomUsersNum = createElement({
    tagName: 'p',
    className: 'room-users-num'
  });
  roomUsersNum.innerText = `${usersNum} users connected`;
  const joinRoomButton = createElement({
    tagName: 'input',
    className: 'join-room-button',
    attributes: {
      id: roomName,
      type: 'button' ,
      value: 'Join'
    }
  });

  room.append(roomUsersNum);
  room.append(roomNameT);
  room.append(joinRoomButton);
  roomsList.append(room);

  return joinRoomButton;
}

export function createProgressList(username, roomName, Rooms, Users) {
  const userList = document.getElementById('user-list');
  let userInfo;
  let wrap;
  Rooms.find(room => room.key === roomName).value.users.forEach(user => {
    userInfo = Users.find(name => name.key === user).value;
    if (user === username) {
      user = `${user}(you)`;
    }
    wrap = createElement({
      tagName: 'div',
      className: 'user-bar-wrap'
    });
    wrap.append(...createBar(user, userInfo.progress, userInfo.isReady));
    userList.append(wrap);
  });
}

function createBar(username, progress, isReady) {
  let element = [];
  element.push(createElement({
    tagName: 'div',
    className: `circle-${isReady ? 'green' : 'red'}`
  }));
  element.push(createElement({
    tagName: 'p',
    className: 'user-progress'
  }));
  element[1].innerText = username;
  element.push(createElement({
    tagName: 'div',
    className: 'progress-bar-wrapper'
  }));
  const bar = createElement({
    tagName: 'div',
    className: 'progress-bar'
  });
  bar.style.width = `${progress}%`;
  element[2].append(bar);

  return element;
}

export function createGameText(textArr) {
  const textEl = createElement({
    tagName: 'p',
    className: 'game-text'
  });
  let gameChar;
  textArr.forEach(char => {
    gameChar = createElement({
      tagName: 'span',
      className: 'game-char'
    });
    gameChar.innerText = char;
    textEl.append(gameChar);
  });
  return textEl;
}